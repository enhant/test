import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default function ParamsTaker() {
	return (
	<Router>
		<>
			<ul>
				<li><Link exact to='/'>Home</Link></li>
				<li><Link to='/about'>about</Link></li>
				<li><Link to='/test'>test</Link></li>
			</ul>
	
			<Route path='/:id' component={ParamTakerTrue} />
		</>
	</Router>
	)
}

function ParamTakerTrue({ match }){
	return (
		<>
			<h1>{match.params.id}</h1>
		</>
	);
}
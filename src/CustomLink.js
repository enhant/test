import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default function CustomLink() {
	return (
		<Router>
		<>
			<StyledLink to="/" activeOnlyWhenExact={true} label="Home"/>
			<StyledLink to="/about" label="About" />
			
			<hr/>

			<Route exact path="/" component={Home}/>
			<Route path="/about" component={About}/>
		</>
		</Router>
	);
}

function StyledLink({ label, to, activeOnlyWhenExact }){
	return (
		<Route
			path={to}
			exact = {activeOnlyWhenExact}
			children = {({match}) => (
			<div className={match ? "active" : ""}>
				{match ? "> " : ""}
				<Link to={to}>{label}</Link>
			</div>
			)}
		/>
	);
}

function Home() {
	return <h3>Home</h3>
}

function About() {
	return <h3>About</h3>
}
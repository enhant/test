import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default function App(){
	return (
		<Router>
			<ul>
				<li><Link to='/'>Home</Link></li>
				<li><Link to='/about'>About</Link></li>
				<li><Link to='/learn'>Learn</Link></li>
			</ul>

			<hr/>

			<Route exact path="/" component={Home} />
			<Route path='/about' component={About}/>
			<Route path='/learn' component={Learn}/>
		</Router>
	);
}

function Home() {
	return (
		<>
			<h1>Home</h1>
		</>
	)
}

function About() {
	return (
		<>
			<h1>About</h1>
		</>
	);
}

function Learn({ match }) {
	return (
	<>
		<ul>
			<li><Link to={`${match.path}/react`}>React</Link></li>
			<li><Link to={`${match.path}/react-router`}>React Router</Link></li>
			<li><Link to={`${match.path}/redux`}>Redux</Link></li>
		</ul>

		<Route path={'${match.path}/:learnId'} component={LearnC}/>
		<Route 
			exact
			path={match.path}
			render = {() => <h1>Choose what u want to learn</h1>}
		/>
	</>
	);
}

function LearnC({ match }){
	return (
		<>
			<h1>{match.params.learnId}</h1>
		</>
	);
}
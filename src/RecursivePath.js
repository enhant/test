import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

const PEEPS = [
	{ id: 0, name: 'Valera', friend: [ 1, 2, 3 ] },
	{ id: 1, name: 'Maslory', friend: [ 0, 2 ] },
	{ id: 2, name: 'Karuman', friend: [ 0, 1 ] },
	{ id: 3, name: 'Dmitriy', friend: [ 0 ] }
]

function find(id){
	return PEEPS.find( p => p.id == id );
}

function Person({ match }) {
	const person = find(match.params.id);
	return (
		<>
			<h3>
				{ person.name } friend list
			</h3>
			<hr/>

			<ul>
				{ person.friend.map(id => (
					<li key={id}>
						<Link to={`${match.url}/${id}`}>{find(id).name}</Link>
					</li>
				)) }
			</ul>
			<Route path={`${match.url}/:id`} component={Person}/>
		</>
	)
}

export default function RecursiveRouter() {
	return (
	<Router>
		<Person match={{ params: { id: 0 }, url: "" }}/>
	</Router>
	)
}
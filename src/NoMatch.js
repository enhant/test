import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';

export default function NoMatch() {
	return (
		<Router>
			<>
				<ul>
					<li><Link to="/">Home</Link></li>
					<li><Link to="/has-no-found">Must be Redirect</Link></li>
					<li><Link to="/will-found">Will found</Link></li>
					<li><Link to="/will-not-match">Have no match</Link></li>
					<li><Link to="/also/will-not-match">Also Have no match</Link></li>
				</ul>
			</>
			<Switch>
				<Route exact path="/" component={Home}/>
				<Redirect from="/has-no-found" to="/will-found"/>
				<Route path="/will-found" component={Found}/>
				<Route component={NoMatched}/>
			</Switch>
		</Router>
	);
}

function Home() {
	return <h3>Home</h3>
}

function Found() {
	return <h3>Founded</h3>
}

function NoMatched({ location }) {
	return (
		<>
			<p>
				Page {location.pathname} doesn't exist
			</p>
		</>
	);
}

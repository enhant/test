import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, withRouter } from 'react-router-dom';

export default function AuthExample() {
	return (
		<Router>
			<>
				<AuthButton/>
				<ul>
					<li><Link to="/public">Public Page</Link></li>
					<li><Link to="/protected">Protected Page</Link></li>
				</ul>
				<Route path="/public" component={Public}/>
				<Route path="/login" component={Login}/>
				<PrivateRoute path="/protected" component={Protected}/>
			</>
		</Router>
	);
}

const fakeAuth = {
	isAuthenticated: false,
	authnticate(cb) {
		this.isAuthenticated = true;
		setTimeout(cb, 100);
	},
	signout(cb) {
		this.isAuthenticated = false;
		setTimeout(cb, 100);
	}
}

const AuthButton = withRouter(
	({ history }) => 
		fakeAuth.isAuthenticated ? (
		<p>
			Welcome!{" "}
			<button
				onClick = {() => {
					fakeAuth.signout( () => history.push('/') );
				}}
			>
				Sign out
			</button>
		</p>
		) : (
			<p>You are logged in</p>
		)
	
);

function PrivateRoute({ component: Component, ...rest }) {
	return (
		<Route
			{...rest}
			render = { props => fakeAuth.isAuthenticated ? (
				<Component {...props}/>
			) : (
				<Redirect
					to={{
						pathname: "/login",
						state: { from: props.location }
					}}
				/>
			) }
		/>
	);
}

function Public() {
	return <h3>Public</h3>
}

function Protected() {
	return <h3>Protected</h3>
}

function Login(props) {
	const [redirectToRender, setRedirectToRender] = useState(false);

	const login = () => {
		setRedirectToRender(true);
	}


	const from = props.location.redirectToRender || { from: { pathname: '/' } }

	return (
		<>
			<p>You must loggin in to view the page at { from.pathname }</p>
			<button onClick={login}>Log in</button>
		</>
	)
}
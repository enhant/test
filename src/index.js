import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import URLparametr from './URLparametr';
import Redirects from './Redirects';
import CustomLink from './CustomLink';
import PreventingTransition from './PreventingTransition';
import NoMatch from './NoMatch';
import RecursivePath from './RecursivePath';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<RecursivePath />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

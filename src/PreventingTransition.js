import React, {useState} from 'react';
import { BrowserRouter as Router, Route, Link, Prompt } from 'react-router-dom';

export default function PreventingTransition() {
	return (
		<Router>
			<>
				<ul>
					<li><Link to ="/">Form</Link></li>
					<li><Link to ="/one">One</Link></li>
					<li><Link to ="/two">Two</Link></li>
				</ul>
			</>
			<Route exact path="/" component={UsefulForm}/>
			<Route path="/one" render= {() => <h2>One</h2>}/>
			<Route path="/two" render= {() => <h2>Two</h2>}/>
		</Router>	
	);
}

function UsefulForm() {
	const [ isBlocked, setBlocked ] = useState(false);

	return (
		<>
			<form
				onSubmit={ event => {
					event.preventDefault();
					event.target.reset();
					setBlocked(false);
				}}
			>
				<Prompt 
					when={isBlocked}
					message = 'Are u sure want to leave this page'
				/>
				<p>
					<p>
						{ isBlocked ? (
								<p>Its Blocked</p>
							) : (
								<p>Its not blocked</p>
							) }
					</p>	
					<input
						size='50'
						message="Type your text"
						onChange={ event => {
							setBlocked(event.target.value.length > 0);
						}}
					/>
				</p>

				<p>
					<button>Click this to clear this form</button>
				</p>
			</form>
		</>
	);	
}